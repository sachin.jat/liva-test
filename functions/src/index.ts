
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as XLSX from "xlsx";
import * as path from "path";
import * as fs from "fs";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const pdf = require("pdf-creator-node");

const monthsString =
 ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

admin.initializeApp();
const db = admin.firestore();
export const bucket = admin.storage().bucket();
export const excelExports = functions.https.onRequest(
    async (request, response) => {
      const user: any[] = [];
      let profile: any[] = [];
      (await db.collection("users").get()).forEach(
          (doc: { data: () => any; id: any }) => {
            const data=doc.data();
            data.id=doc.id;
            user.push(data);
          }
      );
      (await db.collection("profiles").get()).forEach(
          (doc: { data: () => any; id: any }) => {
            const data=doc.data();
            data.id=doc.id;
            profile.push(data);
          }
      );
      const userPlusProfile: any[] = [];
      const mergeById = (a1: any[], a2: any[]) =>
        a1.map((itm) => ({
          ...a2.find((item) => item.id === itm.id && item),
          ...itm,
        }));
      profile = mergeById(profile, user);
      for (const i in profile) {
        if (profile) {
          let birthDate:any =new Date(profile[i]
              ?.birth?._seconds * 1000);
          // const add5hrs=new Date(birthDate
          //     .setHours(birthDate.getHours() + 5));
          // const add30Minutes=new Date(add5hrs
          //     .setMinutes(add5hrs.getMinutes() + 30));
          const birthDateCopy = new Date(profile[i]?.birth?._seconds * 1000);
          const add5hrs=new Date(birthDateCopy
              .setHours(birthDateCopy.getHours() + 5));
          const add30Minutes=new Date(add5hrs
              .setMinutes(add5hrs.getMinutes() + 30));

          let birthTime= add30Minutes.getHours() +":"+
                              add30Minutes.getMinutes() ?? "";

          if (add30Minutes.getHours() >= 0 && add30Minutes.getHours() <= 12) {
            birthTime = `${birthTime} AM`;
          } else {
            birthTime = `${birthTime} PM`;
          }

          if (add30Minutes.getHours() >= 0 && add30Minutes.getHours() < 12) {
            birthDate.setDate(birthDate.getDate() + 1);
          } else {
            birthDate;
          }

          birthDate = birthDateCopy.toLocaleDateString();


          if (add5hrs.getHours() === 0 && add30Minutes.getMinutes() < 30) {
            birthDateCopy.setDate(birthDateCopy.getDate() + 1);
            birthDate = birthDateCopy.toLocaleDateString();
          }


          userPlusProfile.push({
            phone: profile[i].phone ?? "",
            email: profile[i].email ?? "",
            gender: profile[i].gender ?? "",
            maritalStatus: profile[i].maritalStatus ?? "",
            fname: profile[i].fname ?? "",
            mname: profile[i].mname ?? "",
            lname: profile[i].lname ?? "",
            fnameM: profile[i].fnameM ?? "",
            mnameM: profile[i].mnameM ?? "",
            lnameM: profile[i].lnameM ?? "",
            originalFrom: profile[i].originalFrom ?? "",
            taluka: profile[i].taluka ?? "",
            district: profile[i].district ?? "",
            birthName: profile[i].birthName ?? "",
            birth: birthDate + " "+
                                  birthTime,
            birthPlace: profile[i].birthPlace ?? "",
            height: profile[i].height ?? "",
            gotr: profile[i].gotr ?? "",
            varn: profile[i].varn ?? "",
            education: profile[i].education ?? "",
            eduCategory: profile[i].eduCategory ?? "",
            job: profile[i].job ?? "",
            company: profile[i].company ?? "",
            jobPlace: profile[i].jobPlace ?? "",
            salary: profile[i].salary ?? "",
            expectation: profile[i].expectation ?? "",
            unmarriedSisters: profile[i].unmarriedSisters ?? "",
            marriedSisters: profile[i].marriedSisters ?? "",
            unmarriedBrothers: profile[i].unmarriedBrothers ?? "",
            marriedBrothers: profile[i].marriedBrothers ?? "",
            ffatherNameM: profile[i].ffatherNameM ?? "",
            mfatherNameM: profile[i].mfatherNameM ?? "",
            lfatherNameM: profile[i].lfatherNameM ?? "",
            fatherAddress: profile[i].fatherAddress ?? "",
            fatherDesignation: profile[i].fatherDesignation ?? "",
            fatherMobile1: profile[i].fatherMobile1 ?? "",
            fatherMobile2: profile[i].fatherMobile2 ?? "",
            fcontactName1: profile[i].fcontactName1 ?? "",
            mcontactName1: profile[i].mcontactName1 ?? "",
            lcontactName1: profile[i].lcontactName1 ?? "",
            contact1city: profile[i].contact1city ?? "",
            contact1: profile[i].contact1 ?? "",
            fcontactName2: profile[i].fcontactName2 ?? "",
            mcontactName2: profile[i].mcontactName2 ?? "",
            lcontactName2: profile[i].lcontactName2 ?? "",
            contact2city: profile[i].contact2city ?? "",
            contact2: profile[i].contact2 ?? "",
            image: profile[i].image ?? "",
            filledBy: profile[i].filledBy ?? "",
            regNo: profile[i].regNo ?? "",
            status: profile[i].status ?? "",
            createdAt:
                  new Date(profile[i]?.createdAt?._seconds * 1000)
                      .toDateString() ??
                              "",
            hr: profile[i].hr ?? "",
            thumbnail: profile[i].thumbnail ?? "",
            updatedAt:
                      (new Date(profile[i]?.updatedAt?._seconds * 1000)
                          .toDateString()) ??
                              "",
            role: profile[i].role ?? "",
          });
        }
      }
      console.log(profile.length);

      console.log(userPlusProfile.length);
      const userPlusProfileReports = XLSX.utils.json_to_sheet(userPlusProfile);
      const userPlusProfileReportsworkBook = XLSX.utils.book_new();
      const contentType =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
      XLSX.utils.book_append_sheet(
          userPlusProfileReportsworkBook,
          userPlusProfileReports,
          "userPlusProfileReports"
      );
      // XLSX.writeFile(userPlusProfileReportsworkBook, "Report.xls");

      const fileName = "sheets/userPlusProfileReports.xlsx";
      await bucket
          .file(fileName)
          .save(
              XLSX.write(userPlusProfileReportsworkBook, {
                bookType: "xlsx",
                type: "buffer",
              }),
              {
                public: true,
                metadata: {
                  contentType: contentType,
                },
              }
          )
          .then(() =>
            response.status(200).json({
              msg: "working",
            })
          );
    }
);


export const pdfProfileExport = functions.https.onRequest(
    async (request, response) => {
      const id =request.body.id;
      const profile= await db.collection("profiles").doc(id).get();
      const data:any = profile.data();

      const birthDate = new Date(data?.birth?._seconds * 1000);

      console.log("############ birthDate :: ", birthDate);
      console.log("----------------------------");
      if (data.gender === "male") {
        data.type="वराचे नाव";
      } else {
        data.type="वधूचे नाव";
      }


      const birthDateCopy = new Date(data?.birth?._seconds * 1000);

      // data["birthDate"] = birthDate.toLocaleDateString();

      const add5hrs=new Date(birthDateCopy
          .setHours(birthDateCopy.getHours() + 5));
      const add30Minutes=new Date(add5hrs
          .setMinutes(add5hrs.getMinutes() + 30));

      data["birthTime"] = add30Minutes.getHours() +":"+
                        add30Minutes.getMinutes() ?? "";

      if (add30Minutes.getHours() >= 0 && add30Minutes.getHours() <= 12) {
        data.birthTime = `${data.birthTime} AM`;
      } else {
        data.birthTime = `${data.birthTime} PM`;
      }

      // data["birthDate"] = birthDate.toLocaleDateString();
      data["birthDate"] = birthDateCopy.toLocaleDateString();


      if (add5hrs.getHours() === 0 && add30Minutes.getMinutes() < 30) {
        birthDateCopy.setDate(birthDateCopy.getDate() + 1);
        data["birthDate"] = birthDateCopy.toLocaleDateString();
      }


      // if (add30Minutes.getHours() >= 0 && add30Minutes.getHours() < 12) {
      //   birthDate.setDate(birthDate.getDate() + 1);
      //   data["birthDate"] = birthDate.toLocaleDateString();
      // } else {
      //   data["birthDate"] = birthDate.toLocaleDateString();
      // }


      const bdArray = data.birthDate.split("/"); // m - d  - y
      const newDateString =
     `${bdArray[1]} - ${monthsString[parseInt(bdArray[0])-1]} - ${bdArray[2]}`;
      data["birthDate"] = newDateString;


      // Read HTML Template
      const html = fs.readFileSync(path.join(__dirname,
          "./template.html"), "utf8");

      const options = {
        format: "A3",
        orientation: "portrait",
        border: "10mm",
      };

      const document = {
        html: html,
        data: {user: data},
        path: `./files/${id}.pdf`,
        type: "stream",
      };

      console.log("-----------------------------------------------");
      pdf.create(document, options).then(async (res:any) => {
        res.pipe(bucket.file(`pdfs/${id}.pdf`)
            .createWriteStream());
        await db.collection("pdfs").doc(id)
            .set(
                {
                  id: id, url: `https://firebasestorage.googleapis.com/v0/b/leva-matrimonial-test.appspot.com/o/pdfs%2F${id}.pdf?alt=media&`,
                }
            ).then(() =>
              response.send("PDF created and uploaded!"));
      });
    });

export const dataExports = functions.https.onRequest(
    async (request, response) => {
      const user: any[] = [];
      (await db.collection("profiles").get()).forEach(
          (doc: { data: () => any; id: any }) => {
            const data=doc.data();
            data.id=doc.id;
            user.push(data);
          }
      );
      response.send(user);
    });


export const updateProfileCount = functions.firestore
    .document("profiles/{profielId}")
    .onUpdate(async (change, context) => {
      const after = change.after.data();
      const before = change.before.data();
      const id =change.before.id;
      console.log("===============,", after);
      console.log("===============,", before);
      console.log("===============,", id);


      if (before.regNo == 0 && after.regNo !=0 ) {
        const count:any = (await db.collection("masterData")
            .doc("count").get()).data();
        if (after.gender === "male") {
          await db.collection("profiles")
              .doc(id)
              .update({
                regNo: count.maleCount+1,
              });
          await db.collection("masterData").doc("count").update({
            maleCount: count.maleCount+1,
          });
        } else {
          await db.collection("profiles")
              .doc(id)
              .update({
                regNo: count.femaleCount+1,
              });
          await db.collection("masterData").doc("count").update({
            femaleCount: count.femaleCount+1,
          });
        }
      }
    });
